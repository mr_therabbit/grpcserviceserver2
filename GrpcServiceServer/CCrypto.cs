﻿using Google.Protobuf.WellKnownTypes;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using static GrpcServiceServer.ResultReply.Types;
using Org.BouncyCastle.X509.Extension;
using Org.BouncyCastle.Security;
using System.Globalization;

namespace GrpcServiceServer
{
	public class CReturnQualified
	{
		public bool isQualified;
		public List<int> errorCode;
	}
	public class CCrypto
	{
		private List<CSign> sign { get; set; }
		private int errorCode { get; set; }
		private string errorMessage { get; set; }
		private byte[] encodedSignCms { get; set; }
		private byte[] originalFile { get; set; }

		public void LoadFile(string path)
		{
			sign = new List<CSign>();

			errorCode = 0;
			errorMessage = "";

			if (File.Exists(path) == false)
			{
				errorCode = 2;
				errorMessage = "Файл не найден";
				return;
			}

			string signFileContent = File.ReadAllText(path);
			encodedSignCms = null;

			// Если это base64 - то раскодируем его
			bool isDer = IsBase64Encoded(signFileContent);
			if (isDer)
			{
				signFileContent = signFileContent.Replace("-----BEGIN CERTIFICATE-----", string.Empty);
				signFileContent = signFileContent.Replace("-----END CERTIFICATE-----", string.Empty);
				signFileContent = signFileContent.Replace("\r\n", string.Empty);
				encodedSignCms = Convert.FromBase64String(signFileContent);
			}
			else // Если в бинарном виде - то прочитаем файл еще раз в бинарном режиме
			{
				encodedSignCms = File.ReadAllBytes(path);
			}
		}
		public List<CSign> GetSign()
		{
			return sign;
		}

		public int GetErrorCode()
		{
			return errorCode;
		}

		public string GetErrorMessage()
		{
			return errorMessage;
		}

		private string Fingerprint(X509CertificateStructure c)
		{
			byte[] der = c.GetEncoded();
			byte[] sha1 = DigestUtilities.CalculateDigest("SHA1", der);
			byte[] hexBytes = Hex.Encode(sha1);
			return Encoding.ASCII.GetString(hexBytes).ToLower(CultureInfo.InvariantCulture);
		}

		public byte[] GetContent()
		{
			byte[] content = null;
			errorCode = 0;
			try
			{
				CmsSignedData signedData = new CmsSignedData(encodedSignCms);

				CmsSignedDataParser parserSigned = new CmsSignedDataParser(signedData.GetEncoded());

				// Получим из sig файла оригинальный файл originalFile
				using (MemoryStream memoryStream = new MemoryStream())
				{
					parserSigned.GetSignedContent().ContentStream.CopyTo(memoryStream);
					content = memoryStream.ToArray();
					if (content == null || content.Length == 0)
					{
						errorCode = 25;
					}
				}
			}
			catch (Exception e)
			{
				errorCode = 3;
				errorMessage = e.Message;
			}

			return content;
		}

		public void LoadContentSign()
		{
			try
			{
				CmsSignedData signedData = new CmsSignedData(encodedSignCms);

				CmsSignedDataParser parserSigned = new CmsSignedDataParser(signedData.GetEncoded());

				//parserSigned.Drain();
				parserSigned.GetSignedContent().Drain();
				SignerInformationStore signersAll = parserSigned.GetSignerInfos();


				foreach (SignerInformation signer in signersAll.GetSigners())
				{
					CSign currentSign = new CSign();

					currentSign.SerialNumber = ByteArrayToHexString(signer.SignerID.SerialNumber.ToByteArray());
					// Найдем нужный нам сертификат
					ArrayList certCollection = new ArrayList(parserSigned.GetCertificates("Collection").GetMatches(signer.SignerID));
					X509Certificate cert = (X509Certificate)certCollection[0];

					currentSign.Thumbprint = Fingerprint(cert.CertificateStructure);
					currentSign.Version = cert.Version;
					currentSign.ValidFrom = Timestamp.FromDateTimeOffset(cert.NotBefore);
					currentSign.ValidTo = Timestamp.FromDateTimeOffset(cert.NotAfter);
					currentSign.Valid = cert.IsValidNow;
					currentSign.AlgorithmSignature = cert.SigAlgOid;
					currentSign.AlgorithmHash = signer.DigestAlgOid;

					var asd = cert.CertificateStructure.TbsCertificate.Extensions.ExtensionOids;

					// Получим все OID политик сертификата
					DerObjectIdentifier[] certPolicies = cert.CertificateStructure.TbsCertificate.Extensions.GetExtensionOids();
					foreach (DerObjectIdentifier certExtension in certPolicies)
					{
						string valExt = "";
						try
						{
							valExt = cert.CertificateStructure.TbsCertificate.Extensions.GetExtension(certExtension).GetParsedValue().ToString();
						}
						catch { }

						currentSign.OID.Add(new COID
						{
							Oid = certExtension.ToString(),
							Value = valExt
						});
					}

					X509Name subject = cert.SubjectDN;
					currentSign.SubjectName = new CNameCN();
					currentSign.SubjectName.CN = GetValueX509Name(subject, X509Name.CN) ?? "";
					currentSign.SubjectName.E = GetValueX509Name(subject, X509Name.E) ?? "";
					currentSign.SubjectName.SNILS = GetValueX509Name(subject, new DerObjectIdentifier("1.2.643.100.3")) ?? "";
					currentSign.SubjectName.OGRN = GetValueX509Name(subject, new DerObjectIdentifier("1.2.643.100.1")) ?? "";
					if (string.IsNullOrWhiteSpace(currentSign.SubjectName.OGRN))
					{
						currentSign.SubjectName.OGRN = GetValueX509Name(subject, new DerObjectIdentifier("1.2.643.100.5")) ?? "";
						currentSign.SubjectName.IsIp = true;
					}
					currentSign.SubjectName.T = GetValueX509Name(subject, X509Name.T) ?? "";
					currentSign.SubjectName.O = GetValueX509Name(subject, X509Name.O) ?? "";
					currentSign.SubjectName.INN = GetValueX509Name(subject, new DerObjectIdentifier("1.2.643.3.131.1.1")) ?? "";
					if (string.IsNullOrWhiteSpace(currentSign.SubjectName.INN) == false && currentSign.SubjectName.INN.Substring(0, 2) == "00")
					{
						currentSign.SubjectName.INN = currentSign.SubjectName.INN.Substring(2, currentSign.SubjectName.INN.Length - 2);
					}

					currentSign.SubjectName.STREET = GetValueX509Name(subject, X509Name.Street) ?? "";
					currentSign.SubjectName.L = GetValueX509Name(subject, X509Name.L) ?? "";
					currentSign.SubjectName.S = GetValueX509Name(subject, X509Name.ST) ?? "";
					currentSign.SubjectName.C = GetValueX509Name(subject, X509Name.C) ?? "";
					currentSign.SubjectName.G = GetValueX509Name(subject, X509Name.GivenName) ?? "";
					currentSign.SubjectName.SN = GetValueX509Name(subject, X509Name.Surname) ?? "";
					currentSign.SubjectName.OU = GetValueX509Name(subject, X509Name.OU) ?? "";
					currentSign.SubjectName.OID = GetValueX509Name(subject, new DerObjectIdentifier("1.2.840.113549.1.9.2")) ?? "";

					// Получим дату подписи
					try
					{
						string strSignDateTime = signer.SignedAttributes[new DerObjectIdentifier("1.2.840.113549.1.9.5")].AttrValues[0].ToString();
						currentSign.SignDate = Timestamp.FromDateTimeOffset(DateTime.ParseExact(strSignDateTime, "yyMMddHHmmssZ", null));
					}
					catch
					{
					}

					if (currentSign.SubjectName.SN == null && currentSign.SubjectName.G == null && string.IsNullOrWhiteSpace(currentSign.SubjectName.CN) == false && currentSign.SubjectName.CN.IndexOf(" ") >= 0)
					{
						int i = 0;
						foreach (string fio in currentSign.SubjectName.CN.Split(" "))
						{
							if (i == 0) currentSign.SubjectName.SN = fio.Trim();
							else currentSign.SubjectName.G += " " + fio.Trim();
							i++;
						}
						currentSign.SubjectName.G = currentSign.SubjectName.G.Trim();
					}


					Dictionary<string, string> listOID = new Dictionary<string, string>();
					if (string.IsNullOrWhiteSpace(currentSign.SubjectName.OID) == false) listOID = ParseDns(currentSign.SubjectName.OID);
					if (listOID != null && listOID.Count > 0)
					{
						if (currentSign.SubjectName.OGRN == null && listOID.ContainsKey("OGRN") && listOID["OGRN"] != null) currentSign.SubjectName.OGRN = listOID["OGRN"];
						if (currentSign.SubjectName.INN == null && listOID.ContainsKey("INN") && listOID["INN"] != null) currentSign.SubjectName.INN = listOID["INN"];
						if (listOID.ContainsKey("KPP") && listOID["KPP"] != null) currentSign.SubjectName.KPP = listOID["KPP"];
					}
					if (string.IsNullOrWhiteSpace(currentSign.SubjectName.SN) == false) currentSign.SubjectName.LastName = currentSign.SubjectName.SN;
					if (string.IsNullOrWhiteSpace(currentSign.SubjectName.G) == false)
					{
						int i = 0;
						foreach (string name in currentSign.SubjectName.G.Split(" "))
						{
							if (i == 0) currentSign.SubjectName.FirstName = name.Trim();
							else if (i == 1) currentSign.SubjectName.MiddleName = name.Trim();
							else currentSign.SubjectName.MiddleName += " " + name.Trim();
							i++;
						}
					}

					if (string.IsNullOrWhiteSpace(currentSign.SubjectName.S) == false)
					{
						try
						{
							int i = 0;
							string s = "";
							foreach (string word in currentSign.SubjectName.S.Split(" "))
							{
								if (i == 0) currentSign.SubjectName.Region = Convert.ToInt32(word.Trim());
								else if (i == 1) s = word.Trim();
								else s += " " + word.Trim();
								i++;
							}
							if (s.Length > 0) currentSign.SubjectName.S = s;
						}
						catch
						{
						}
					}

					// Проверим, чей это сертификат - банка или клиента
					if (checkIsBankSign(currentSign)) currentSign.IsBank = true;
					else currentSign.IsClient = true;


					X509Name issuer = cert.IssuerDN;
					currentSign.IssuerName = new CNameCN();
					currentSign.IssuerName.CN = GetValueX509Name(issuer, X509Name.CN) ?? "";
					currentSign.IssuerName.E = GetValueX509Name(issuer, X509Name.E) ?? "";
					currentSign.IssuerName.SNILS = GetValueX509Name(issuer, new DerObjectIdentifier("1.2.643.100.3")) ?? "";
					currentSign.IssuerName.OGRN = GetValueX509Name(issuer, new DerObjectIdentifier("1.2.643.100.1")) ?? "";
					if (string.IsNullOrWhiteSpace(currentSign.IssuerName.OGRN) == true) currentSign.IssuerName.OGRN = GetValueX509Name(issuer, new DerObjectIdentifier("1.2.643.100.5")) ?? "";
					currentSign.IssuerName.T = GetValueX509Name(issuer, X509Name.T) ?? "";
					currentSign.IssuerName.O = GetValueX509Name(issuer, X509Name.O) ?? "";
					currentSign.IssuerName.INN = GetValueX509Name(issuer, new DerObjectIdentifier("1.2.643.3.131.1.1")) ?? "";
					currentSign.IssuerName.STREET = GetValueX509Name(issuer, X509Name.Street) ?? "";
					currentSign.IssuerName.L = GetValueX509Name(issuer, X509Name.L) ?? "";
					currentSign.IssuerName.S = GetValueX509Name(issuer, X509Name.ST) ?? "";
					currentSign.IssuerName.C = GetValueX509Name(issuer, X509Name.C) ?? "";
					currentSign.IssuerName.G = GetValueX509Name(issuer, X509Name.GivenName) ?? "";
					currentSign.IssuerName.SN = GetValueX509Name(issuer, X509Name.Surname) ?? "";
					currentSign.IssuerName.OU = GetValueX509Name(issuer, X509Name.OU) ?? "";
					currentSign.IssuerName.OID = GetValueX509Name(issuer, new DerObjectIdentifier("1.2.840.113549.1.9.2")) ?? "";

					// Проверим квалифицированную подпись
					CReturnQualified resultQualified = checkQualifiedSign(currentSign);
					currentSign.Qualified = resultQualified.isQualified;
					currentSign.QualifiedErrors.AddRange(resultQualified.errorCode);

					sign.Add(currentSign);

				}
			}
			catch (Exception e)
			{
				errorCode = 3;
				errorMessage = e.Message;
			}
		}

		public Dictionary<string, string> ParseDns(string data)
		{
			if (data == null) return null;
			if (!data.Contains("=")) return null;
			Dictionary<string, string> result = new Dictionary<string, string>();

			foreach (string str in data.Split("/"))
			{
				if (str.Contains("="))
				{
					string[] ln = str.Split("=");
					if (ln[0] != null && ln[0].Length > 0) ln[0] = ln[0].ToUpper().Trim();
					if (ln[1] != null && ln[1].Length > 0) ln[1] = ln[1].Trim();
					if (ln[0] != null && ln[0].Length > 0 && ln[1] != null && ln[1].Length > 0)
					{
						if (ln[0] == "INN" || ln[0] == "ИНН" || ln[0] == "INNIP" || ln[0] == "ИННИП")
						{
							result.Add("INN", ln[1]);
						}
						else if (ln[0] == "OGRN" || ln[0] == "ОГРН" || ln[0] == "OGRNIP" || ln[0] == "ОГРНИП")
						{
							result.Add("OGRN", ln[1]);
						}
						else if (ln[0] == "KPP" || ln[0] == "КПП")
						{
							result.Add("KPP", ln[1]);
						}
					}
				}
			}
			return result;
		}

		private string GetValueX509Name(X509Name subject, DerObjectIdentifier OID)
		{
			string result = null;
			try
			{
				IList item = subject.GetValueList(OID);
				if (item.Count > 0)
				{
					result = item[0].ToString().Trim();
				}

			}
			catch
			{
			}
			return result;
		}

		private string ByteArrayToHexString(byte[] Bytes)
		{
			StringBuilder Result = new StringBuilder(Bytes.Length * 2);
			string HexAlphabet = "0123456789ABCDEF";

			foreach (byte B in Bytes)
			{
				Result.Append(HexAlphabet[(int)(B >> 4)]);
				Result.Append(HexAlphabet[(int)(B & 0xF)]);
			}

			return Result.ToString().ToLower(CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Проверяет - это файл в base64?
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		private bool IsBase64Encoded(String str)
		{
			try
			{
				Convert.FromBase64String(str);
				return Regex.IsMatch(str, @"[^-A-Za-z0-9+/=]|=[^=]|={3,}$", RegexOptions.IgnoreCase);
			}
			catch
			{
				return false;
			}

		}

		/// <summary>
		/// Проверка сертификата - это банка или клиента?
		/// </summary>
		/// <param name="signResult"></param>
		/// <returns></returns>
		private bool checkIsBankSign(CSign signResult)
		{
			try
			{
				if (signResult.SubjectName.OGRN == "1027700218666" || signResult.SubjectName.OGRN == "1027700218666") return true;
			}
			catch
			{
			}
			return false;
		}

		private CReturnQualified checkQualifiedSign(CSign signResult)
		{
			//bool qualified = true;
			CReturnQualified qualified = new CReturnQualified { isQualified = true, errorCode = new List<int>() };
			/*
			Цитата из рекомендаций Минсвязи:
			Каждый СКПЭП должен содержать следующие атрибуты и расширения (в указанном порядке):
			1.	Версия (version) – должна быть не ниже 3.
			2.	Серийный номер (serial number).
			3.	Алгоритм подписи (signature) – в поле algorithm должен содержаться идентификатор алгоритма подписи ГОСТ Р 34.11-94/34.10-2001 (OID.1.2.643.2.2.3, в соответствии с RFC4491).
			4.	Имя издателя СКПЭП (issuer) – данные из поля «Имя владельца» соответствующего СКПЭП УЦ. См. 3.4 Состав имени издателя СКПЭП.
			5.	Дата и время начала действия СКПЭП (notBefore).
			6.	Дата и время окончания действия СКПЭП (notAfter).
			7.	Имя владельца СКЭП (subject). См. 3.3 Состав имени субъекта.
			8.	Открытый ключ (subjectPublicKeyInfo).
			9.	Дополнения (расширения) сертификата (Extensions). Должны содержаться следующие дополнения (порядок данным документом не регламентируется):
			9.1.	Authority Key Identifier, OID.2.5.29.35, идентификатор ключа УЦ.
			9.2.	Key Usage, OID.2.5.29.15, область использования ключа.
			9.3.	Certificate Policies, OID.2.5.29.32, политики сертификата (см. Приложение 12).
			9.4.	Subject Sign Tool, OID.1.2.643.100.111, средство ЭП владельца сертификата.
			9.5.	Issuer Sign Tool, OID.1.2.643.100.112, средство ЭП УЦ.
			9.6.	ExtendedKeyUsage, OID.2.5.29.37, расширенное использования ключа. Состав дополнения зависит от информационной системы, в которой используется СКПЭП.
			9.7.	CDP, OID.2.5.29.31, точки распространения списков отзыва.
			Кроме того, СКПЭП может содержать другие дополнения (не критические), в зависимости от требований конкретных информационных систем, в которых они используются.

			{{ "oid": "2.5.29.37", "value": "[1.3.6.1.5.5.7.3.4, 1.2.643.2.2.34.6, 1.3.6.1.5.5.7.3.2, 1.2.643.2.2.34.25, 1.2.643.100.113.1]" }}
			*/
			if (signResult.Version != 3)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(4);
			}
			if (signResult.SerialNumber.Length == 0)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(5);
			}
			// "1.2.643.2.2.3"	Алгоритм цифровой подписи ГОСТ Р 34.10-2001
			// "1.2.643.7.1.1.3.2" Алгоритм цифровой подписи ГОСТ Р 34.10 - 2012 для ключей длины 256 бит
			// "1.2.643.7.1.1.3.3" Алгоритм цифровой подписи ГОСТ Р 34.10 - 2012 для ключей длины 512 бит
			/*if (signResult.algorithmSignature != "1.2.643.2.2.3" && signResult.algorithmSignature != "1.2.643.7.1.1.3.2" && signResult.algorithmSignature != "1.2.643.7.1.1.3.3")
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(3);
			}*/

			if (signResult.AlgorithmSignature != "1.2.643.7.1.1.3.2" && signResult.AlgorithmSignature != "1.2.643.7.1.1.3.3")
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(6);
			}
			if (signResult.Valid == false)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(7);
			}
			if (signResult.SubjectName.CN.Length == 0)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(8);
			}
			if (signResult.SubjectName.O.Length == 0 && signResult.SubjectName.INN.Length != 12)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(9);
			}
			if (signResult.SubjectName.INN.Length == 0)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(10);
			}
			if (signResult.SubjectName.OGRN.Length == 0)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(11);
			}
			if (signResult.IssuerName.CN.Length == 0)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(12);
			}
			if (signResult.IssuerName.O.Length == 0)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(13);
			}
			if (signResult.IssuerName.INN.Length == 0)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(14);
			}
			if (signResult.IssuerName.OGRN.Length == 0)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(15);
			}

			if (!signResult.OID.Any(x => x.Oid == "2.5.29.35"))
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(16);
			}
			if (!signResult.OID.Any(x => x.Oid == "2.5.29.15"))
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(17);
			}
			if (!signResult.OID.Any(x => x.Oid == "2.5.29.32"))
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(18);
			}
			if (!signResult.OID.Any(x => x.Oid == "1.2.643.100.111"))
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(19);
			}
			if (!signResult.OID.Any(x => x.Oid == "1.2.643.100.112"))
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(20);
			}
			if (!signResult.OID.Any(x => x.Oid == "2.5.29.37"))
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(21);
			}
			if (!signResult.OID.Any(x => x.Oid == "2.5.29.31"))
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(22);
			}

			if (signResult.ValidFrom.ToDateTime() > DateTime.Now)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(23); // Сертификат еще не наступил по времени
			}

			if (signResult.ValidTo.ToDateTime() < DateTime.Now)
			{
				qualified.isQualified = false;
				qualified.errorCode.Add(24); // Сертификат истек
			}

			return qualified;
		}
	}
}
