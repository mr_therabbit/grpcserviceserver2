﻿using Flurl.Http.Configuration;
using MihaZupan;
using System;
using System.Net;
using System.Net.Http;

namespace GrpcServiceServer
{
	public class ProxyClientFactory : DefaultHttpClientFactory
	{
		private string _type;
		private string _host;
		private int _port;
		private string _address;
		private string _username;
		private string _password;

		public ProxyClientFactory(string type, string host, int port, string username = null, string password = null)
		{
			_type = type;
			_host = host;
			_port = port;
			if (type == "http") _address = "http://" + _host + ":" + _port.ToString();
			_username = username;
			_password = password;
		}

		public override HttpMessageHandler CreateMessageHandler()
		{
			if (_type == "http")
			{
				NetworkCredential proxyCreds = null;
				var proxyUri = new Uri("http://" + _host + ":" + _port.ToString());
				if (_username != null && _password != null)
				{
					proxyCreds = new NetworkCredential(_username, _password);
				}
				var clientHandler = new HttpClientHandler
				{
					UseProxy = true,
					Proxy = new WebProxy(proxyUri, false)
					{
						UseDefaultCredentials = false,
						Credentials = proxyCreds
					},
					PreAuthenticate = true,
					AllowAutoRedirect = true,
					UseDefaultCredentials = false
				};
				if (_username != null && _password != null)
				{
					clientHandler.Credentials = proxyCreds;
				}
				return clientHandler;
			}
			else if (_type == "socks")
			{
				HttpToSocks5Proxy socksProxy = null;
				if (_username == null && _password == null) socksProxy = new HttpToSocks5Proxy(_host, _port);
				else socksProxy = new HttpToSocks5Proxy(_host, _port, _username, _password);
				return new HttpClientHandler { Proxy = socksProxy };
			}
			return null;
		}
	}
}
