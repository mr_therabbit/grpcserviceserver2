﻿using Flurl;
using Flurl.Http;
using System;
using System.Threading.Tasks;

namespace GrpcServiceServer
{
	class CRuCaptchaResponse
    {
        public int status { get; set; }
        public string request { get; set; }
    }
    public class CRuCaptchaClient
    {
        private string _token = null;
        private string urlBase = "http://rucaptcha.com/";
        private string idCaptcha = null;
        public bool isError = false;
        public string codeError = null;
        public ProxyClientFactory _proxyFactory = null;
        //FlurlClient proxyHttpClient = null;

        public CRuCaptchaClient(string token, CItemProxy proxy)
        {
            _token = token;
            Console.WriteLine("Выбрана прокси для RuCaptcha: {0} {1} {2}", proxy.type, proxy.host, proxy.port);
            _proxyFactory = new ProxyClientFactory(proxy.type, proxy.host, proxy.port, proxy.username, proxy.password);
        }

        public async Task sendBase64Captcha(string imgBase64, int numeric, int min_len, int max_len)
        {
            using (var cli = new FlurlClient(urlBase).Configure(settings => { settings.HttpClientFactory = _proxyFactory; }))
            {
                try
                {
                    CRuCaptchaResponse rIn = await cli.Request("in.php").PostUrlEncodedAsync(new
                    {
                        method = "base64",
                        key = _token,
                        numeric = numeric,
                        min_len = min_len,
                        max_len = max_len,
                        json = 1,
                        body = imgBase64
                    }).ReceiveJson<CRuCaptchaResponse>();

                    if (rIn.status == 1) idCaptcha = rIn.request;
                    else
                    {
                        isError = true;
                        codeError = rIn.request;
                    }
                }
                catch (FlurlHttpException ex)
                {
                    Console.WriteLine("Ошибка в sendBase64Captcha: {0}", ex.Message);
                }

                
            }
        }

        public async Task<string> getResult(int cntSec = 5)
        {
            string result = null;
            using (var cli = new FlurlClient(urlBase).Configure(settings => { settings.HttpClientFactory = _proxyFactory; }))
            {
                while (true)
                {
                    try
                    {
                        // Подождем 5 секунд
                        await Task.Delay(cntSec * 1000);

                        // Сделаем GET запрос на получение результата
                        CRuCaptchaResponse rRes = await cli.Request("res.php").SetQueryParams(new
                        {
                            key = _token,
                            action = "get",
                            id = idCaptcha,
                            json = 1
                        }).GetJsonAsync<CRuCaptchaResponse>();

                        if (rRes.status == 1)
                        {
                            result = rRes.request;
                            break;
                        }
                        else if (rRes.request != "CAPCHA_NOT_READY")
                        {
                            isError = true;
                            codeError = rRes.request;
                            break;
                        }
                    }
                    catch (FlurlHttpException ex)
                    {
                        Console.WriteLine("Ошибка в getResult: {0}", ex.Message);
                    }
                }
            }
            return result;
        }

        public async Task report(bool isGoodOrBad)
        {
            if (idCaptcha != null)
            {
                using (var cli = new FlurlClient(urlBase).Configure(settings => { settings.HttpClientFactory = _proxyFactory; }))
                {
                    try
                    {
                        await cli.Request("res.php").SetQueryParams(new
                        {
                            key = _token,
                            action = (isGoodOrBad ? "reportgood" : "reportbad"),
                            id = idCaptcha
                        }).GetStringAsync();
                    }
                    catch (FlurlHttpException ex)
                    {
                        Console.WriteLine("Ошибка в report: {0}", ex.Message);
                    }
                }
            }
        }

    }
}
