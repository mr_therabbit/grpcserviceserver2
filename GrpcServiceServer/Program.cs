using AngleSharp.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace GrpcServiceServer
{
	public class CMetibConfig
	{
		public string[] proxy { get; set; }
	}

	public class Program
	{
		public static IConfiguration appConfig { get; set; }
		public static CMetibConfig metibConfig { get; set; }
		public static Dictionary<int, DateTime?> libreofficeUsePort { get; set; } // ����� ����� / ����� ����� ������ ���������� �������������
		public static AutoResetEvent waitLibreofficeUsePortHandler = new AutoResetEvent(true);

		public static void Main(string[] args)
		{
			// ������� ������������
			appConfig = ModulStudio.Utils.GetConfigurationFromJsonFile("appsettings.json");

			// ������� �� redis ������������ ������� � ������ ������
			ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(appConfig["redis:connect"]);
			IDatabase db = redis.GetDatabase(Convert.ToInt32(appConfig["redis:db"]));
			string metibConfigJson = db.StringGet("metib:config");
			metibConfig = JsonConvert.DeserializeObject<CMetibConfig>(metibConfigJson);
			redis.Close();

			// �������� ������� � ������� ��� ����������� ������
			libreofficeUsePort = new Dictionary<int, DateTime?>();

			List<int> tmpPorts = new List<int>();
			appConfig.GetSection("unoconv:ports").Bind(tmpPorts);
			foreach (int p in tmpPorts)
			{
				libreofficeUsePort.Add(p, null);
			}

			CreateHostBuilder(args).Build().Run();
		}

		// Additional configuration is required to successfully run gRPC on macOS.
		// For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682
		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				});
	}
}
