﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using ModulStudio;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GrpcServiceServer.ResultReply.Types;

namespace GrpcServiceServer.Services
{
	public class EchoService : Echo.EchoBase
	{
		public EchoService(ILogger<EchoService> logger)
		{
		}

		public override Task<StatReply> Stat(StatRequest request, ServerCallContext context)
		{
			StatReply result = new StatReply()
			{
				Echo = request.Echo,
				GrpcServiceServer = true,
				MetibWorkersNATS = false,
				MetibCksResponse = false,
				StorageData01 = false,
				StorageData02 = false,
				CntLibreOffice = 0
			};

			if (File.Exists("/mnt/data01/checkmount.txt")) result.StorageData01 = true;
			if (File.Exists("/mnt/data02/checkmount.txt")) result.StorageData02 = true;

			// Получим список процессов
			// pgrep -f MetibWorkersNATS

			// Найдем процесс с MetibWorkersNATS
			result.MetibWorkersNATS = UtilsProcess.GetPidProcessByName("MetibWorkersNATS").Count > 0;
			// Найдем процесс с MetibCksResponse
			result.MetibCksResponse = UtilsProcess.GetPidProcessByName("MetibCksResponse").Count > 0;
			// Найдем процесс с MetibCksResponse
			result.CntLibreOffice = UtilsProcess.GetPidProcessByName("soffice.bin").Count;

			return Task.FromResult(result);
		}
	}
}
