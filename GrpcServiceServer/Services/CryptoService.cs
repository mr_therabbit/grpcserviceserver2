using Grpc.Core;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static GrpcServiceServer.ResultReply.Types;

namespace GrpcServiceServer
{
	public class CryptoService : Crypto.CryptoBase
	{
		private readonly ILogger<CryptoService> _logger;
		public CryptoService(ILogger<CryptoService> logger)
		{
			_logger = logger;
		}

		/// <summary>
		/// �������� ���� �� ���������
		/// </summary>
		/// <param name="request"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public override Task<ResultReply> SendFile(FileRequest request, ServerCallContext context)
		{
			CCrypto crypto = new CCrypto();
			crypto.LoadFile(request.Path);
			crypto.LoadContentSign();


			ResultReply result = new ResultReply() {
				Error = new CError()
			};
			if (crypto.GetErrorCode() > 0)
			{
				result.Error.Code = crypto.GetErrorCode();
				result.Error.Message = crypto.GetErrorMessage();
			}
			else
			{
				result.CntSign = crypto.GetSign().Count();
				result.Sign.AddRange(crypto.GetSign());
			}

			return Task.FromResult(result);
		}

		/// <summary>
		/// �������� �� sig ����� ������������ ����
		/// </summary>
		/// <param name="request"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public override Task<GetContentReply> GetContent(GetContentRequest request, ServerCallContext context)
		{
			GetContentReply result = new GetContentReply()
			{
				Error = new CError()
			};

			CCrypto crypto = new CCrypto();
			crypto.LoadFile(request.Path);
			byte[] content = null;
			if (crypto.GetErrorCode() == 0) content = crypto.GetContent();
			if (crypto.GetErrorCode() > 0)
			{
				result.Error.Code = crypto.GetErrorCode();
				result.Error.Message = crypto.GetErrorMessage();
			}
			else
			{
				result.Error.Code = 0;
				// �������� ����
				File.WriteAllBytes(request.PathContent, content);
			}
			return Task.FromResult(result);
		}
	}
}
