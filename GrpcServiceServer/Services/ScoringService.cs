﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using static GrpcServiceServer.FsspReply.Types;

namespace GrpcServiceServer.Services
{
	public class ScoringService : Scoring.ScoringBase
	{
		private readonly ILogger<ScoringService> _logger;
		public ScoringService(ILogger<ScoringService> logger)
		{
			_logger = logger;
		}

		public override Task<FsspReply> CheckFssp(FsspRequest request, ServerCallContext context)
		{
			CFssprus fssp = new CFssprus();
			Task<List<FssprusResponse>> taskFssp = Task.Run(async () => await fssp.searchPhysical(request.Lastname, request.Firstname, request.Secondname, request.Birthdate));

			FsspReply reply = new FsspReply();
			reply.Result.AddRange(taskFssp.Result);

			return Task.FromResult(reply);
		}
	}
}
