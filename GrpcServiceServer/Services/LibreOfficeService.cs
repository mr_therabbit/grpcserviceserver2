﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrpcServiceServer.Services
{
	public class LibreOfficeService : LibreOffice.LibreOfficeBase
	{
		private string host = null;
		private int reservePort;

		public LibreOfficeService(ILogger<LibreOfficeService> logger)
		{
			host = Program.appConfig["unoconv:host"];
			reservePort = 0;
		}

		public override Task<SuccessReply> Convert(PathConvertRequest request, ServerCallContext context)
		{
			// Получим расширения файлов
			string extensionInput = Path.GetExtension(request.InputFile).ToLower();
			string extensionOutput = Path.GetExtension(request.OutputFile).ToLower();

			bool isCreateFile = false;
			Console.WriteLine("InputFile {0} / {1}, OutputFile {2} / {3}", request.InputFile, extensionInput, request.OutputFile, extensionOutput);

			if (string.IsNullOrWhiteSpace(request.InputFile) || !File.Exists(request.InputFile))
			{
				isCreateFile = false;
			}
			else if (request.InputFile == request.OutputFile)
			{
				isCreateFile = true;
			}
			else if (extensionInput == extensionOutput) // Если файлов одинаковые расширения - то просто скопируем файлы
			{
				File.Copy(request.InputFile, request.OutputFile);
				isCreateFile = true;
			}
			else
			{
				string convertToFormat = "pdf";
				if (extensionOutput == ".docx") convertToFormat = "docx7";

				string tmpInputFile = ModulStudio.Utils.GetPathTmpFile(Program.appConfig["tmp"], extensionInput);
				string tmpOutputFile = ModulStudio.Utils.GetPathTmpFile(Program.appConfig["tmp"], extensionOutput);

				try
				{
					File.Copy(request.InputFile, tmpInputFile);

					while (isCreateFile == false)
					{
						// Резервируем порт
						ReservePort();

						// Удаляем конечный файл если он есть
						if (File.Exists(tmpInputFile) && File.Exists(request.OutputFile)) File.Delete(request.OutputFile);

						try
						{
							/*
							 --no-launch - По умолчанию, если слушатель не запущен, unoconv запустит собственный (временный) слушатель, чтобы убедиться, что преобразование работает. 
							Эта опция прервет преобразование, если слушатель не будет найден, вместо того, чтобы запустить наш собственный слушатель.
							 */

							// Запускаем конвертатор
							ProcessStartInfo startInfo = new ProcessStartInfo
							{
								FileName = "/usr/bin/unoconv",
								Arguments = "--server=" + host + " --port=" + reservePort + " --no-launch --format=" + convertToFormat + " --output=" + tmpOutputFile + " " + tmpInputFile,
								UseShellExecute = false,
								StandardOutputEncoding = Encoding.UTF8,
								RedirectStandardOutput = true,
								CreateNoWindow = true,
								RedirectStandardError = true,
							};

							using (Process exeProcess = Process.Start(startInfo))
							{
								exeProcess.WaitForExit();
							}
						}
						catch (Exception e)
						{
							throw e;
						}

						// Освобождаем порт
						ReleasePort();

						// Проверяем создался ли файл
						if (File.Exists(tmpOutputFile) && new FileInfo(tmpOutputFile).Length > 0)
						{
							isCreateFile = true;
							File.Copy(tmpOutputFile, request.OutputFile, true); // Копируем его в выходной файл
							File.Delete(tmpOutputFile); // Удаляем временный
						}
						else
						{
							Thread.Sleep(2000);
						}
					}
					// Удалим временный входящий файл
					if (File.Exists(tmpInputFile)) File.Delete(tmpInputFile);
				}
				catch
				{
					isCreateFile = false;
				}
			}

			return Task.FromResult(new SuccessReply
			{
				Success = isCreateFile
			});
		}

		/// <summary>
		/// Резервирует порт.
		/// </summary>
		private void ReservePort()
		{
			while (reservePort == 0)
			{
				Program.waitLibreofficeUsePortHandler.WaitOne();
				reservePort = Program.libreofficeUsePort.Where(x => (x.Value == null || x.Value <= DateTime.Now)).FirstOrDefault().Key;

				if (reservePort > 0) // порт найден
				{
					Program.libreofficeUsePort[reservePort] = DateTime.Now.AddMinutes(2); // Блокируем порт на 2 минуты, на время исполнения задания
				}

				Program.waitLibreofficeUsePortHandler.Set();
			}
		}

		/// <summary>
		/// Освобождает выделенный порт.
		/// </summary>
		private void ReleasePort()
		{
			if (reservePort > 0)
			{
				Program.waitLibreofficeUsePortHandler.WaitOne();

				Program.libreofficeUsePort[reservePort] = null;

				Program.waitLibreofficeUsePortHandler.Set();
			}
			reservePort = 0;
		}
	}
}
