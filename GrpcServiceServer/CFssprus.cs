﻿using AngleSharp;
using Flurl.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using static GrpcServiceServer.FsspReply.Types;

namespace GrpcServiceServer
{
	public class CFsspAjaxResponse
	{
		public string data { get; set; }
		public string err { get; set; }
	}
	public class CItemProxy
	{
		public string type { get; set; }
		public string host { get; set; }
		public int port { get; set; }
		public string username { get; set; }
		public string password { get; set; }
	}
	public class CFssprus
	{
		private CItemProxy proxy = null;
		public ProxyClientFactory _proxyFactory = null;
		private Random random = new Random();

		private IFlurlClient cli = null;
		private int cntUseProxy = 0;
		private int cntMaxUseProxy = 5;
		private CItemProxy localProxy = null;

		public CFssprus()
		{
			localProxy = new CItemProxy
			{
				type = Program.appConfig["proxy:type"],
				host = Program.appConfig["proxy:host"],
				port = Convert.ToInt32(Program.appConfig["proxy:port"]),
				username = (Program.appConfig["proxy:username"].Length > 0 ? Program.appConfig["proxy:username"] : null),
				password = (Program.appConfig["proxy:password"].Length > 0 ? Program.appConfig["proxy:password"] : null),
			};
		}

		private void resetProxy()
		{
			cntUseProxy = 0;
			proxy = null;
		}

		public void getProxy()
		{
			if (cntUseProxy >= cntMaxUseProxy) resetProxy();

			if (proxy == null)
			{
				// Сначала применим локальную
				proxy = localProxy;

				try
				{
					if (Program.metibConfig.proxy.Length > 0)
					{
						// Получим случайную прокси
						int posI = random.Next(0, Program.metibConfig.proxy.Length);
						string strProxy = Program.metibConfig.proxy[posI];
						Console.WriteLine(strProxy);

						// Распарсим строку вида AFw8w9:Kcxjco@185.233.203.203:9263
						string[] s1 = strProxy.Split('@');
						string[] sUP = s1[0].Split(':');
						string[] sHP = s1[1].Split(':');
						proxy = new CItemProxy
						{
							type = "socks",
							host = sHP[0],
							port = Convert.ToInt32(sHP[1]),
							username = sUP[0],
							password = sUP[1],
						};
					}
				}
				catch
				{
					Console.WriteLine("Исключение в получении прокси из redis");
				}

				_proxyFactory = new ProxyClientFactory(proxy.type, proxy.host, proxy.port, proxy.username, proxy.password);

				cli = new FlurlClient("https://is.fssprus.ru").Configure(settings => { settings.HttpClientFactory = _proxyFactory; }).EnableCookies()
					.WithHeaders(new
					{
						Accept = "*/*",
						User_Agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36",
						Accept_Encoding = "gzip, deflate, br",
						Accept_Language = "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
						Cache_Control = "no-cache",
						Connection = "keep-alive",
						DNT = 1,
						Host = "is.fssprus.ru",
						Pragma = "no-cache",
						Referer = "http://fssprus.ru/iss/ip",
						Sec_Fetch_Dest = "script",
						Sec_Fetch_Mode = "no-cors",
						Sec_Fetch_Site = "cross-site"
					});
			}
		}

		public async Task<List<FssprusResponse>> searchPhysical(string lastname, string firstname, string secondname, string birthdate)
		{
			List<FssprusResponse> resultSearch = new List<FssprusResponse>();

			string jqCallback = "jQuery" + ModulStudio.Utils.RandomStringNumeric(20) + "_" + ModulStudio.Utils.RandomStringNumeric(13);
			string tt = ModulStudio.Utils.RandomStringNumeric(13);
			string imgBase64Capcha = null;
			string codeCapcha = null;

			CItemProxy proxyCaptcha = localProxy;
			CRuCaptchaClient capcha = new CRuCaptchaClient(Program.appConfig["rucaptcha_key"], proxyCaptcha);

			while (true)
			{
				// Получим прокси, который будем использовать
				getProxy();
				Console.WriteLine("Выбрана прокси для ФССП: {0} {1} {2}", proxy.type, proxy.host, proxy.port);

				// Капча была найдена на предыдущем шаге
				if (imgBase64Capcha != null)
				{
					// Отошлем капчу
					Console.WriteLine("Отсылаем на распознание капчу");
					await capcha.sendBase64Captcha(imgBase64Capcha, 4, 5, 5);
					codeCapcha = await capcha.getResult();
					Console.WriteLine("Получили результат распознания капчи: {0}", codeCapcha);
					if (capcha.isError == true) // Если есть ошибка от капчи - сделаем выход
					{
						Console.WriteLine("Ошибка от rucapcha {0}", capcha.codeError);
						return resultSearch;
					}
				}

				var url = new Flurl.Url("ajax_search").SetQueryParams(new
				{
					callback = jqCallback,
					system = "ip",
					nocache = 1,
					_ = tt,
					code = codeCapcha
				});

				url.QueryParams["is%5Bextended%5D"] = 1;
				url.QueryParams["is%5Bvariant%5D"] = 1;
				url.QueryParams["is%5Bregion_id%5D%5B0%5D"] = -1;
				url.QueryParams["is%5Blast_name%5D"] = lastname;
				url.QueryParams["is%5Bfirst_name%5D"] = firstname;
				url.QueryParams["is%5Bpatronymic%5D"] = secondname;
				url.QueryParams["is%5Bdrtr_name%5D"] = "";
				url.QueryParams["is%5Bip_number%5D"] = "";
				url.QueryParams["is%5Bdate%5D"] = birthdate;
				url.QueryParams["is%5Baddress%5D"] = "";
				url.QueryParams["is%5Bid_number%5D"] = "";
				url.QueryParams["is%5Bid_type%5D%5B0%5D"] = "";
				url.QueryParams["is%5Bid_issuer%5D"] = "";

				Console.WriteLine("GET {0}", url);

				cntUseProxy++;

				try
				{
					string ajaxJson = await cli.Request(url).GetStringAsync();

					// Найдем выражение /**/ typeof jQuery34005829950246752356_1582819438801 === 'function' && jQuery34005829950246752356_1582819438801({"data":"\r\n\r\n<div id=
					int pos1 = ajaxJson.IndexOf("&& " + jqCallback + "(");
					ajaxJson = ajaxJson.Remove(0, pos1 + jqCallback.Length + 4);
					// удалим вконце );
					ajaxJson = ajaxJson.Remove(ajaxJson.Length - 2);
					// Распарсим JSON
					CFsspAjaxResponse dataJson = JsonConvert.DeserializeObject<CFsspAjaxResponse>(ajaxJson);

					var config = Configuration.Default;
					var context = BrowsingContext.New(config);
					var document = await context.OpenAsync(req => req.Content(dataJson.data));

					// Проверим, может у нас запросили капчу
					var captchaPopup = document.QuerySelector("#captcha-popup");
					if (captchaPopup != null)
					{
						// Проверим, может нам сказали что капча не правильная?
						if (dataJson.data.IndexOf("Неверно введен код") > -1)
						{
							Console.WriteLine("Капча не правильная");
							// Тогда отошлем извещение, чтобы вернули деньги за капчу
							await capcha.report(false);
						}

						var imgCaptcha = document.QuerySelector("img");
						if (imgCaptcha != null)
						{
							Console.WriteLine("С нас требуют капчу");
							imgBase64Capcha = imgCaptcha.GetAttribute("src");
							// Отправим капчу на распознование
							if (imgBase64Capcha.Length > 0) imgBase64Capcha = imgBase64Capcha.Replace("data:image/jpeg;base64,", "");
							else imgBase64Capcha = null;
						}
					}
					else // Капчи нет, получаем результаты
					{
						// Капча принята, отошлем извещение, что капча правильная
						if (imgBase64Capcha != null) await capcha.report(true);
						imgBase64Capcha = null;

						// Проверим пустые результаты
						var devEmpty = document.QuerySelector(".empty");
						// Результаты есть, получим их
						if (devEmpty == null)
						{
							var trResult = document.QuerySelectorAll(".results .results-frame table tr");
							int i = 0;
							foreach (var itemTr in trResult)
							{
								if (i > 0) // Игнорируем заголовок
								{
									if (itemTr.ClassList.Contains("region-title") == false) // Игнорируем список регионов
									{
										int j = 0;
										FssprusResponse res = new FssprusResponse();
										var tdResult = itemTr.QuerySelectorAll("td");
										if (tdResult != null && tdResult.Length == 8)
										{
											foreach (var itemTd in tdResult)
											{
												string innerText = itemTd.InnerHtml.Replace("<br />", "\n", StringComparison.OrdinalIgnoreCase).
													Replace("<br/>", "\n", StringComparison.OrdinalIgnoreCase).
													Replace("<br>", "\n", StringComparison.OrdinalIgnoreCase).
													Replace("<br >", "\n", StringComparison.OrdinalIgnoreCase).Trim().Trim('\n');

												if (j == 0) res.Name = innerText;
												else if (j == 1) res.ExeProductionStr = innerText;
												else if (j == 2) res.Details = innerText;
												else if (j == 3) res.IpEnd = innerText;
												else if (j == 5) res.Subject = innerText;
												else if (j == 6) res.Department = innerText;
												else if (j == 7) res.Bailiff = innerText;
												else if (j == 4) // столбец сервис
												{
													var aResult = itemTd.QuerySelector(".pay-wrap a");
													if (aResult != null)
													{
														res.ExeProduction = aResult.GetAttribute("data-ip");
														string dr = aResult.GetAttribute("debt-rest");
														if (dr.Length > 0) res.DebtRest = float.Parse(dr, CultureInfo.InvariantCulture.NumberFormat);
													}
												}
												j++;
											}
											resultSearch.Add(res);
										}
									}
								}
								i++;
							}
						}
						Console.WriteLine("Получено {0} исполнительных производств", resultSearch.Count);
						break;
					}
				}
				catch (FlurlHttpException ex)
				{
					if (ex.Message.IndexOf("Call failed with status code 504 (Gateway Time-out)") != -1)
					{
						resultSearch.Add(new FssprusResponse() {
							Name = "Сервис ФССП не работает. Попробуйте позже."
						});

						return resultSearch;
					}

					Console.WriteLine("Ошибка в searchPhysical: {0}", ex.Message);
					resetProxy();
				}


			}
			return resultSearch;
		}
	}
}
